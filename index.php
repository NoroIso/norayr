<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Soc</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="login.css">
</head>
<body>
	<canvas id='c'></canvas>
	<div id="page">
		<div class="wrap">
  		<h2 class="login">Registration!</h2>
  		<div class="shady-form">
   		 	<div id="logbar">
				<input type="text" id="name" >
    			<label class="name"  for="name">Name</label>
				<input type="text" id="surname" >
    			<label class="surname" for="surname">Surname</label>
				<input type="text" id="age" >
    			<label class="age" for="age">Age</label>
				<input type="text" id="email" >
    			<label class="email" for="email">Email</label>
				<input type="text" id="password" >
    			<label class="password" for="password">Password</label>
				<input type="text" id="confirm_password" >
    			<label class="confirm_password" for="confirm_password">Confirm Password</label>
				<br>
     			 <div class="btn_wrap">
				<button  id="save">Save</button>
     			 </div>
   				 </div>
  			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="script.js"></script>
<script type="text/javascript" src="login.js"></script>
</html>
