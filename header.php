<!DOCTYPE html>
<html>
<title>Soc</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="profil.css">
<link rel="stylesheet" type="text/css" href="photos.css">
<link rel="stylesheet" type="text/css" href="search.css">
<link rel="stylesheet" type="text/css" href="friends.css">
<link rel="stylesheet" type="text/css" href="request.css">
<link rel="stylesheet" type="text/css" href="footer.css">
<?php session_start();
  if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
  }
  else{
    header('location:login.php');
  }
?>
<body class="w3-black w3-content" style="max-width:1600px">
  
<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-black w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container">
    <a  id="menclose" class="w3-hide-large w3-right w3-jumbo w3-padding w3-hover-grey" title="close menu">
      <i class="fa fa-remove"></i>
    </a>
    <img id="my_pic1" src="<?php print ($user['profile_picture']);?>" style="width:45%;" class="w3-round"><br><br>
    <h4 class="pgname" style= "color:red"><b><?php print ($user['name']." ".$user['surname']); ?></b></h4>
    <p class="w3-text-blue pgname"><?php print ("Age:"." ".$user['age']);?></p>
  </div>
  <div class="w3-bar-block">
    <a href="profil.php"  class="w3-bar-item w3-button w3-padding w3-text-teal"><i class="fa fa-th-large fa-fw w3-margin-right"></i>Home</a>
     <!--  -->
    <a href="request.php"  class="w3-bar-item w3-button w3-padding"><i class="fa fa-bell fa-fw w3-margin-right"></i>NOTIFICATION<span class="qanak w3-badge w3-right w3-small w3-green"></span></a>
    <!--  -->
    <a href="photos.php"  class="w3-bar-item w3-button w3-padding"><i class="fa fa-picture-o fa-fw w3-margin-right"></i>PHOTOS</a> 
    <a href="friends.php" class="w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw w3-margin-right"></i>FRIENDS</a>
    <a href="edit.php"  class="w3-bar-item w3-button w3-padding"><i class="fa fa-pencil fa-fw w3-margin-right"></i>EDIT</a> 
    <a href="search.php"  class="w3-bar-item w3-button w3-padding"><i class="fa fa-search fa-fw w3-margin-right"></i>SEARCH</a>
    <a id="logout" class="w3-bar-item w3-button w3-padding"><i class="fa fa-sign-out fa-fw w3-margin-right" aria-hidden="true"></i>LOG OUT</a>
  </div>
  <div class="w3-panel w3-large">
    <i class="fa fa-facebook-official  w3-white w3-hover-opacity"></i>
    <i class="fa fa-instagram  w3-white w3-hover-opacity"></i>
    <i class="fa fa-snapchat  w3-white w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p  w3-white w3-hover-opacity"></i>
    <i class="fa fa-twitter  w3-white w3-hover-opacity"></i>
    <i class="fa fa-linkedin  w3-white w3-hover-opacity"></i>
  </div>
</nav>
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px">
  <!-- Header -->
  <header id="portfolio">
    <a href="#"><img src="" style="width:65px;" class="w3-circle w3-right w3-margin w3-hide-large w3-hover-opacity"></a>
    <span id="menop" class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey"><i class="fa fa-bars"></i></span>
