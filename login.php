<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Soc</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="login.css">
</head>
<body>
	<canvas id='c'></canvas>
	<div id="page">
		<div class="wrap">
  		<h2 class="login">Lets Login!</h2>
  		<div class="shady-form">
   		 	<div id="logbar">
     			 <input name="username" id="email" type="text">
    			  <label class="email" for="email">Email</label>
     			 <input name="pass" id="password" type="password">
     			 <label class="password" for="password">Password</label>
     			 <div class="btn_wrap">
      			 <input value="Lets Go!" title="Submit" type="submit" id="sub">
     			 </div>
   				 </div>
   		 	<div class="sign-up">Looks like you're new here! <a href="index.php">Registration!</a></div>
  			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="loginfunc.js"></script>
<script type="text/javascript" src="login.js"></script>
</html>
