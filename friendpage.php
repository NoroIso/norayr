<?php require_once("header.php");?> 
    <div class="w3-container">
    <h1 id="frpr" class="pgname" style="float: right;"></h1>
    </div>
  </header>
<hr>

<!-- Photo grid -->
<button id="pictoggle">Pictures</button>
<button id="frtoggle">Friends</button>
  <div id="frpho-gallery" class="w3-row" style="display: none;">
  </div>

  <!-- Modal for full size images on click-->
  <div id="modal01" class="w3-modal w3-black" style="padding-top:0;z-index: 150;" onclick="this.style.display='none'">
    <span class="w3-button w3-black w3-xlarge w3-display-topright">×</span>
    <div class="w3-modal-content w3-animate-zoom w3-center w3-transparent w3-padding-64">
      <img id="img01" class="w3-image">
      <p id="caption"></p>
    </div>
  </div>
<hr>

  <div id="fr_friendlist" class="w3-row" style="display: none;"></div>


<div id="profpg" class="w3-container w3-padding-large ">
    <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
       <!-- Middle Column -->
    <div class="w3-col m7">
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
          </div>
        </div>
      </div>
       <!--  -->
      <div id="frpostbody"></div>
      <!--  -->
    <!-- End Middle Column -->
    </div>
    </div>
  </div>
<input type="hidden" id = 'id_fr' value = "<?php print $_GET['id'] ?>">
<?php require_once("footer.php");?> 
<script type="text/javascript" src= 'friendpage.js'></script>
</html>
 

