$.ajax({
  url:"server.php",
  type:"post",
  data:{ action:'ajax6'},
  success:function(r){
    r = JSON.parse(r)
    r.forEach(function(item){
    $("#photo-gallery").append(`
             <div id="picdiv" class="w3-third"  >
                  <img id="picimg" src="${item['photo']}"  style="width:100%" onclick="onClick(this)">
                     <button id="deletephoto" value='${item['id']}' class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Delete</button>
                  <button id="profilepicture" value='${item['photo']}' class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Change Profil Picture</button>
             </div>`) 
    })
  }
});
$(document).on("click","#deletephoto",function(){
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to recover this imaginary file!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
  let photo=$(this).val();
  $.ajax({
    url:"server.php",
    type:"post",
    data:{ action:'ajax7',photo},
    success:function(r){
      location.reload()
      console.log(r)
    }
});
    swal("Poof! Your imaginary file has been deleted!", {
      icon: "success",
    });
  } else {
    swal("Your imaginary file is safe!");
  }
});
});
$(document).on("click","#profilepicture",function(){
  let photo=$(this).val();
  $.ajax({
    url:"server.php",
    type:"post",
    data:{ action:'ajax8',photo},
    success:function(r){
     location.reload()
    }
});
}); 
function onClick(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
  var captionText = document.getElementById("caption");
  captionText.innerHTML = element.alt;
}
