<?php require_once("header.php");?> 
    <div class="w3-container">
    <h1 class="pgname"><b>Search</b></h1>
    </div>
  </header>
    <div class="w3-container w3-padding-large ">
    <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
    <hr class="w3-opacity">
      <div class="search-container">
		<input type="text" id="search-bar" placeholder="What can I help you with today?">
			</div >
		<div id="d1" class="w3-row"></div>
	</div>  
  </div>
<?php require_once("footer.php");?> 
<script type="text/javascript" src="search.js"></script>
</html>

