<?php require_once("header.php");?>
    <div class="w3-container" style="height: 200px">
    <h1 class="pgname" style="display: inline-block;"><b>Photos</b></h1>
    <div class="wrapper-block">
<form enctype="multipart/form-data" action = "server.php" method = "post" class="photoform">
  <input type="file" name="photo"  accept="image/*" value="sss" class="fa fa-camera" aria-hidden="true"><br>
  <button name="savephoto"  class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Save</button>
</form>
</div>
</div>
  </header>
  <!-- Photo grid -->
  <div id="photo-gallery" class="w3-row" >
  </div>
  <!-- Pagination -->
  <div class="w3-center w3-padding-32">
    <div class="w3-bar">
      <a href="#" class="w3-bar-item w3-button w3-hover-black">«</a>
      <a href="#" class="w3-bar-item w3-black w3-button">1</a>
      <a href="#" class="w3-bar-item w3-button w3-hover-black">2</a>
      <a href="#" class="w3-bar-item w3-button w3-hover-black">3</a>
      <a href="#" class="w3-bar-item w3-button w3-hover-black">4</a>
      <a href="#" class="w3-bar-item w3-button w3-hover-black">»</a>
    </div>
  </div>
  <!-- Modal for full size images on click-->
  <div id="modal01" class="w3-modal w3-black" style="padding-top:0;z-index: 150;" onclick="this.style.display='none'">
    <span class="w3-button w3-black w3-xlarge w3-display-topright">×</span>
    <div class="w3-modal-content w3-animate-zoom w3-center w3-transparent w3-padding-64">
      <img id="img01" class="w3-image">
      <p id="caption"></p>
    </div>
  </div>
<?php require_once("footer.php");?> 
<script type="text/javascript" src="photos.js"></script>
</html>

