$("#search-bar").on("input",function() {
let inp=$("#search-bar").val();
  $("#d1").empty();
    if(inp!=""){      
     $.ajax({
         url:"server.php",
         type:"post",
         data:{action:"ajax4",srch:inp},
         success:function(r){
                 r=JSON.parse(r);
                  r.forEach( function(item) {
                    if (item['status']==0) {
                     $("#d1").append(`
                         <div id="srchdiv" class="w3-third"  > 
                           <div class="box">
                             <div class="card">
                              <div class="card">
                                <div class="imgBx">
                                 <img src="${item["profile_picture"]}" alt="images">
                              </div>
                            <div class="details">
                          <h2>${item["name"]} ${item["surname"]} <br><span>Age:${item["age"]}</span></h2>
                        </div>
                      </div>
                    </div>
                  </div>
                      <button id="addfriend" value='${item['id']}' class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right">Add Friend</i></button>
                </div>
              </div>`);
             }   
            else if (item['status']==1) {
                     $("#d1").append(`
                         <div id="srchdiv" class="w3-third"  > 
                           <div class="box">
                             <div class="card">
                              <div class="card">
                                <div class="imgBx">
                                 <img src="${item["profile_picture"]}" alt="images">
                              </div>
                            <div class="details">
                          <h2>${item["name"]} ${item["surname"]} <br><span>Age:${item["age"]}</span></h2>
                        </div>
                      </div>
                    </div>
                  </div>
                      <div id="frd1" value='${item['id']}' class="badge badge-success">Friend</div>
                </div>
              </div>`);
             }   
             else if (item['status']==2) {
                     $("#d1").append(`
                         <div id="srchdiv" class="w3-third"  > 
                           <div class="box">
                             <div class="card">
                              <div class="card">
                                <div class="imgBx">
                                 <img src="${item["profile_picture"]}" alt="images">
                              </div>
                            <div class="details">
                          <h2>${item["name"]} ${item["surname"]} <br><span>Age:${item["age"]}</span></h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id='d2'>
                      <button id="requested" value='${item['id']}' class="badge badge-secondary">Requested</button>
                 </div>
                </div>
              </div>`);
             }   
             else if (item['status']==3) {
                     $("#d1").append(`
                         <div id="srchdiv" class="w3-third"  > 
                           <div class="box">
                             <div class="card">
                              <div class="card">
                                <div class="imgBx">
                                 <img src="${item["profile_picture"]}" alt="images">
                              </div>
                            <div class="details">
                          <h2>${item["name"]} ${item["surname"]} <br><span>Age:${item["age"]}</span></h2>
                        </div>
                      </div>
                    </div>
                  </div>
                    <button id="acceptreq" value='${item['id']}' class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Accept</button>
                    <button id="deletereq" value='${item['id']}' class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Delete</button>
                </div>
              </div>`);
             }
            });  
         }
       })
      }
});
$(document).on("click","#addfriend",function(){
  let add_id=$(this).val();
  let th = $(this)
  $.ajax({
    url:"server.php",
    type:"post",
    data:{ action:'ajax9',add_id},
    success:function(r){
     th.replaceWith( `
              <button id="requested" value='add_id' class="badge badge-secondary">Requested</button>
    ` );
    }
});
});
$(document).on("click","#requested",function(){
  let req_id=$(this).val();
  let th = $(this)

    $.ajax({
    url:"server.php",
    type:"post",
    data:{ action:'ajax18',req_id},
    success:function(r){
  th.replaceWith( `
                      <button id="addfriend" value='req_id' class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right">Add Friend</i></button>
    ` );
    }
});
});
