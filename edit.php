<?php require_once("header.php");?>
    <div class="w3-container">
    <h1 class="pgname"><b>Edit</b></h1>
    </div>
  </header>
    <div class="w3-container w3-padding-large w3-grey">
    <h4 id="contact"><b>Change Information</b></h4>
    <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
    <hr class="w3-opacity">
      <div class="w3-section">
        <label class="name"  for="name">Name</label>
        <input id="name" value="<?php print ($user['name']); ?>" class="w3-input w3-border" type="text" name="Name" required>
      </div>
      <div class="w3-section">
        <label class="surname" for="surname">Surame</label>
        <input id="surname" value="<?php print ($user['surname']); ?>" class="w3-input w3-border" type="text" required>
      </div>
      <div class="w3-section">
        <label class="age" for="age">Age</label>
        <input type="text" id="age" value="<?php print ($user['age']); ?>" class="w3-input w3-border" type="text" name="Message" required>
      </div>
      <button  id="savechanges" class="w3-button w3-round w3-black w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Save</button>
  </div>
  </div>
<?php require_once("footer.php");?> 
<script type="text/javascript" src="profil.js"></script> 
<script type="text/javascript" src="edit.js"></script>
</html>

