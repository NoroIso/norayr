<?php require_once("header.php");?> 
    <div class="w3-container">
    <h1 class="pgname"><b>Profile Page</b></h1>
    </div>
  </header>
    <div id="profpg" class="w3-container w3-padding-large ">
    <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
       <!-- Middle Column -->
    <div class="w3-col m7">
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <h6 class="w3-opacity"></h6>
              <form action="server.php" method="post" enctype="multipart/form-data">
              <textarea id="postarea" contenteditable="true" name="posttext"></textarea>
              <input id="postphoto" type="file" name="postphoto"  accept="image/*" class=" w3-button w3-theme fa fa-camera" aria-hidden="true">
              <button id="sharepost" type="submit" class="w3-button w3-theme" name = 'addpost'><i class="fa fa-pencil"></i>  Share</button> 
              </form>
            </div>
          </div>
        </div>
      </div>
       <!--  -->
      <div id="postbody"></div>
      <!--  -->
    <!-- End Middle Column -->
    </div>
    </div>
  </div>
<?php require_once("footer.php");?> 
</html>
 

