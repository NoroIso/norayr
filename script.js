$(document).on("click","#save",function(){
	let name=$("#name").val();
	let surname=$("#surname").val();
	let age=$("#age").val();
	let email=$("#email").val();
	let pass=$("#password").val();
	let conf_pass=$("#confirm_password").val();
	$.ajax({
		url:"server.php",
		type:"post",
		data:{ action:'ajax1',name,surname,age,email,pass,conf_pass},
		success:function(r){
			if (r) {
			$('.error').remove()
			$('input').css({border:"1px solid #ced4da"})
			$('.password').css({background:"rgb(176, 211, 235)",border:"1px solid #ced4da"}).html("Password")
			$('.email').css({background:"rgb(176, 211, 235)",border:"1px solid #ced4da"}).html("Email")
			$('.age').css({background:"rgb(176, 211, 235)",border:"1px solid #ced4da"}).html("Age")
			$('.name').css({background:"rgb(176, 211, 235)",border:"1px solid #ced4da"}).html("Name")
			$('.surname').css({background:"rgb(176, 211, 235)",border:"1px solid #ced4da"}).html("Surname")
			$('.confirm_password').css({background:"rgb(176, 211, 235)",border:"1px solid #ced4da"}).html("Confirm Password")
			 r=JSON.parse(r);
			 for(i in r){
			  $("#"+i).css({border:"2px solid #ff5454"})
			  $("."+i).html(`${r[i]}`).css({background:"#ff5454",border:"2px solid #ff5454"});
			 }
			}else{
				$(location).attr('href','login.php');
			}
		}
});
});