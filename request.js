$.ajax({
	url:"server.php",
	type:"post",
	data:{ action:'ajax10'},
	success:function(r){
		 r = JSON.parse(r)
		 r.forEach(function(item){
		 $("#requestlist").append(`
			       <div id="reqdiv" class="w3-third" >
      		          <img id="reqpic1img" src="${item['profile_picture']}">
                      <h4 class="pgname">${item["name"]} ${item["surname"]}</h4>
   			          <button id="acceptreq" value='${item['id']}' class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Accept</button>
    			      <button id="deletereq" value='${item['id']}' class="w3-button w3-round bttt w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Delete</button>
             </div>`)	
		 })
	}
});
