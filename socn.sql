/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : socn

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 03/03/2020 13:46:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES (1, 1, 1, '', '2020-02-26 22:22:02');
INSERT INTO `comments` VALUES (2, 1, 1, 'Yeah', '2020-02-26 22:22:12');
INSERT INTO `comments` VALUES (3, 2, 2, 'Yeah', '2020-02-26 22:30:39');
INSERT INTO `comments` VALUES (4, 1, 2, '', '2020-02-26 22:31:33');
INSERT INTO `comments` VALUES (5, 1, 2, '', '2020-02-26 22:31:35');
INSERT INTO `comments` VALUES (6, 1, 2, '', '2020-02-26 22:32:11');
INSERT INTO `comments` VALUES (7, 1, 2, '', '2020-02-26 22:32:21');
INSERT INTO `comments` VALUES (8, 2, 1, 'Wow', '2020-03-01 00:17:14');
INSERT INTO `comments` VALUES (9, 1, 1, '', '2020-03-01 00:17:25');
INSERT INTO `comments` VALUES (10, 1, 2, '', '2020-03-01 00:41:59');
INSERT INTO `comments` VALUES (11, 1, 2, 'hhh', '2020-03-01 00:42:03');
INSERT INTO `comments` VALUES (12, 2, 1, '', '2020-03-02 12:43:51');
INSERT INTO `comments` VALUES (13, 1, 2, 'Hello swetie', '2020-03-02 12:57:03');
INSERT INTO `comments` VALUES (14, 3, 1, 'Thank you', '2020-03-02 13:47:55');
INSERT INTO `comments` VALUES (15, 3, 3, 'yeah', '2020-03-02 13:48:50');
INSERT INTO `comments` VALUES (16, 3, 1, '', '2020-03-03 13:41:46');

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `my_id`(`my_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of friends
-- ----------------------------
INSERT INTO `friends` VALUES (1, 2, 1);
INSERT INTO `friends` VALUES (2, 2, 1);
INSERT INTO `friends` VALUES (3, 3, 1);
INSERT INTO `friends` VALUES (4, 3, 1);
INSERT INTO `friends` VALUES (5, 3, 2);
INSERT INTO `friends` VALUES (6, 3, 2);
INSERT INTO `friends` VALUES (7, 4, 1);
INSERT INTO `friends` VALUES (8, 4, 1);
INSERT INTO `friends` VALUES (9, 4, 2);
INSERT INTO `friends` VALUES (10, 4, 2);
INSERT INTO `friends` VALUES (11, 4, 3);
INSERT INTO `friends` VALUES (12, 4, 3);

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) NOT NULL,
  `rec_id` int(11) NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `my_id`(`my_id`) USING BTREE,
  INDEX `rec_id`(`rec_id`) USING BTREE,
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `message_ibfk_2` FOREIGN KEY (`rec_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES (1, 1, 2, 'Hello', '2020-02-26 22:26:54');
INSERT INTO `message` VALUES (2, 2, 1, 'Hi', '2020-02-26 22:27:45');

-- ----------------------------
-- Table structure for photos
-- ----------------------------
DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of photos
-- ----------------------------
INSERT INTO `photos` VALUES (5, 2, 'images/1582741446daenerys-targaryen-in-game-of-thrones-tv-series-1r.jpg');
INSERT INTO `photos` VALUES (6, 2, 'images/1582741453daenerys-targaryen-game-of-thrones-4k-vm.jpg');
INSERT INTO `photos` VALUES (7, 2, 'images/1582741459daenerys-targaryen-with-his-dragon-9d.jpg');
INSERT INTO `photos` VALUES (8, 2, 'images/1582741465daenerys-targayen-with-dragons-artwork-bl.jpg');
INSERT INTO `photos` VALUES (9, 2, 'images/1582741478emilia-clarke-2.jpg');
INSERT INTO `photos` VALUES (10, 2, 'images/1582741488emilia-clarke-4.jpg');
INSERT INTO `photos` VALUES (11, 2, 'images/1582741496emilia-clarke.jpg');
INSERT INTO `photos` VALUES (12, 2, 'images/1582741505emilia-clarke-as-daenerys-targaryen-art-d0.jpg');
INSERT INTO `photos` VALUES (13, 2, 'images/1582741515emilia-clarke-hd.jpg');
INSERT INTO `photos` VALUES (14, 2, 'images/1582741524mother-of-dragons-jp.jpg');
INSERT INTO `photos` VALUES (15, 2, 'images/1582741532wings-daenerys-targayen-r1.jpg');
INSERT INTO `photos` VALUES (18, 1, 'images/1582912001artwork-game-of-thrones-season-8-va.jpg');
INSERT INTO `photos` VALUES (19, 1, 'images/1582912009dragons-fight-game-of-thrones-season-8-ba.jpg');
INSERT INTO `photos` VALUES (20, 1, 'images/1582912016battle-of-dragons-game-of-thrones-8k-ge.jpg');
INSERT INTO `photos` VALUES (21, 1, 'images/1582912029night-king-vs-jon-snow-4k-1t.jpg');
INSERT INTO `photos` VALUES (22, 1, 'images/1582912039a-song-of-ice-and-fire-tp.jpg');
INSERT INTO `photos` VALUES (24, 3, 'images/1582912280em1.jpg');
INSERT INTO `photos` VALUES (26, 3, 'images/1582912294e8.jpg');
INSERT INTO `photos` VALUES (27, 3, 'images/1582912304em2.jpg');
INSERT INTO `photos` VALUES (28, 3, 'images/1582912310em3.jpg');
INSERT INTO `photos` VALUES (29, 3, 'images/1582912317em9.jpg');
INSERT INTO `photos` VALUES (30, 3, 'images/1582912325em11.jpg');

-- ----------------------------
-- Table structure for post_like
-- ----------------------------
DROP TABLE IF EXISTS `post_like`;
CREATE TABLE `post_like`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `post_like_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post_like
-- ----------------------------
INSERT INTO `post_like` VALUES (1, 1, 1);
INSERT INTO `post_like` VALUES (2, 2, 2);
INSERT INTO `post_like` VALUES (3, 1, 2);
INSERT INTO `post_like` VALUES (4, 2, 1);
INSERT INTO `post_like` VALUES (5, 1, 2);
INSERT INTO `post_like` VALUES (6, 3, 2);
INSERT INTO `post_like` VALUES (7, 3, 1);
INSERT INTO `post_like` VALUES (8, 3, 3);

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, 1, 'All the Hail Jon Snow the true king of Westeros', '2020-02-26 22:21:53', 'images/1582741313artwork-game-of-thrones-season-8-va.jpg');
INSERT INTO `posts` VALUES (2, 2, 'Yes All my king ', '2020-02-26 22:30:30', 'images/1582741830jon-snow-and-khalessi-love-cosplay-4k-cb.jpg');
INSERT INTO `posts` VALUES (3, 2, 'All hail to the Jon Snow true king of seven kingdoms', '2020-03-02 13:46:36', 'images/1583142396battle-of-dragons-game-of-thrones-8k-ge.jpg');

-- ----------------------------
-- Table structure for request
-- ----------------------------
DROP TABLE IF EXISTS `request`;
CREATE TABLE `request`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `my_id`(`my_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `request_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `request_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of request
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `surname` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `age` int(11) NOT NULL,
  `profile_picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Norayr', 'Isahakyan', 'norayrisahakyan@gmail.com', '$2y$10$ZOg.LWBJkZC9im4rbPe5OODMhHp3z6VAjwR305myzXJRvytazyLNK', 20, 'images/1582912029night-king-vs-jon-snow-4k-1t.jpg');
INSERT INTO `users` VALUES (2, 'Lilit', 'Ketikyan', 'ketikyan.lilit9@gmail.com', '$2y$10$a/Q2DUQ.bKz2kcMGs53PnuwCIFIApiX85mwhnxzVJuV5qf/c0tZIe', 20, 'images/1582741505emilia-clarke-as-daenerys-targaryen-art-d0.jpg');
INSERT INTO `users` VALUES (3, 'Hasmik', 'Petrosyan', 'hasopetrosyan98@gmail.com', '$2y$10$T.BHzXNpnemCff3abkXi9eChcJpFOTh7BMJk1nLJ72jK8j1JCAZn2', 21, 'images/1582912280em1.jpg');
INSERT INTO `users` VALUES (4, 'Sargis', 'Hovhanissyan', 'saqohovh12@gmail.com', '$2y$10$3QB0MAL6ATvGDNvjQL/0YO3JA.BVguJE4qDoxWDoVCz6/OuNCW/MG', 21, 'profile_picture.png');

SET FOREIGN_KEY_CHECKS = 1;
